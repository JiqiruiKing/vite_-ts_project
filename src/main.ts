import { createApp } from "vue";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import App from "./App.vue";
import SvgIconVue from "./components/SvgIcon/SvgIcon.vue";
import "../src/assets/iconfont/iconfont.js";
import router from "./router/index.ts"; //引入路由组件信息💻
import i18n from "./i18n/index.ts"; //引入i18n
import { setupStore } from "./store/index.ts";

const app = createApp(App);
app.component("SvgIcon", SvgIconVue);
setupStore(app); //引入pinia
// 注册 Pinia 插件
app.use(ElementPlus);
app.use(router);
app.use(i18n);
app.mount("#app");
