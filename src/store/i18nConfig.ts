/**
 * @description 国际化配置文件
 */
import { useI18n } from "vue-i18n";
import { defineStore } from "pinia";
import { ref } from "vue";

interface ILanguage {
  language: string;
}

export const useLangStore = defineStore("lang", () => {
  const language = ref<string>(sessionStorage.getItem("localeLang") || "znCh");
  const { locale } = useI18n(); //切换i18n的语言

  const changeLang = (data: "zhCn" | "en") => {
    console.log("切换了吗", data);
    language.value = data;
    sessionStorage.setItem("localeLang", data);
    console.log(sessionStorage.getItem("localeLang"));
    locale.value = data;
  };

  return {
    language,
    changeLang,
  };
});

// export const useLangStore = defineStore("lang", {
//   state: (): ILanguage => {
//     return {
//       language: sessionStorage.getItem("localeLang") || "znCh",
//     };
//   },

//   actions: {
//     changeLang(data: "zhCn" | "en") {
//       this.language = data;
//     },
//   },
//   persist: true,
// });
