/**
 * @description 测试数据
 */

import { ref } from "vue";
import { defineStore } from "pinia";

export const useTestData = defineStore("testData", () => {
  // 定义测试数据信息
  const tsData = ref("测试数据信息");
  //定义测试的方法
  const testHandle = () => {
    tsData.value += "测试数据信息";
  };
  return {
    tsData,
    testHandle,
  };
});
