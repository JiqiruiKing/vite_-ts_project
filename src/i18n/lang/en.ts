export default {
  btn: {
    login: "Login",
    logout: "Logout",
    register: "Register",
    test: "testbtn",
  },
  home: {
    name: "home",
  },
  tabs: {
    work: "Work",
    private: "Private",
    collect: "Collect",
    like: "Like",
  },
};
