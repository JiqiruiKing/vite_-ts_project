export default {
  btn: {
    login: "登录",
    logout: "退出",
    register: "注册",
    test: "测试",
  },
  home: {
    name: "首页",
  },
  tabs: {
    work: "作品",
    private: "私密",
    collect: "收藏",
    like: "喜欢",
  },
};
