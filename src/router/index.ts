/**
 * @description 路由配置
 */
import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'


//默认正常必须要有的路由页面
const normalRoutes = [
    {
        path: '/',
        name: 'home',
        component: () => import("../views/home/index.vue")
    },
    {
        path: '/login',
        name: 'login',
        component: () => import("../views/login/index.vue")
    }
]
//后期需要展示的一些路由页面信息
const asyncRoutes: [] = [
]

// 合并路由
const routes = [...normalRoutes, ...asyncRoutes]


const router = createRouter({
    history: createWebHistory(),//创建路由地址信息
    routes
})

export default router