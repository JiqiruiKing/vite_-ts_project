import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  // 设置启动端口号
  server: {
    port: 8099, //可以设置端口号
  },
  // 设置路径别名
  resolve: {
    alias: {
      "@": "/src",
    },
  },
});
